import pandas
from pandas import read_csv


def get_dataframe_ratings_base(text):
    """
    đọc file base của movilens, lưu thành dataframe với 3 cột user id, item id, rating
    """
    r_cols = ['user_id', 'item_id', 'rating']
    ratings = pandas.read_csv(text, sep='\t', names=r_cols, encoding='latin-1')
    # print(ratings)
    
    iduser = dict(enumerate(ratings.user_id.unique()))
    userid = {y:x for x,y in iduser.items()}
    ratings.user_id = ratings.user_id.apply(lambda x: userid[x])
    idmovie = dict(enumerate(ratings.item_id.unique()))
    moveid = {y:x for x,y in idmovie.items()}
    ratings.item_id = ratings.item_id.apply(lambda x: moveid[x])

    Y_data = ratings.values
    return Y_data,  userid, idmovie

def get_dataframe_ratings():
    """
    đọc file base của movilens, lưu thành dataframe với 3 cột user id, item id, rating
    """
    ratings = pandas.read_csv('dataset/data.csv')
    # print(ratings)
    
    iduser = dict(enumerate(ratings.user_id.unique()))
    userid = {y:x for x,y in iduser.items()}
    ratings.user_id = ratings.user_id.apply(lambda x: userid[x])
    idmovie = dict(enumerate(ratings.movie_id.unique()))
    moveid = {y:x for x,y in idmovie.items()}
    ratings.movie_id = ratings.movie_id.apply(lambda x: moveid[x])

    Y_data = ratings.values
    return Y_data,  userid, idmovie


# def get_name_movie(text):
#     """
#     lấy danh sách tên phim theo file u.item của movielens
#     """
#     r_cols = ['name', 'year', 'imdb', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19','20','21','22']
#     list_movie = pandas.read_csv(text, sep='|', names=r_cols, encoding='latin-1')
#     list_name_movie = list_movie['name'].values
#     return list_name_movie


# def get_year_movie(text):
#     """
#     lấy danh sách năm sản xuất phim theo file u.item của movielens
#     """
#     r_cols = ['name', 'year', 'imdb', '3','4', '5', '6','7', '8', '9','10', '11', '12','13', '14', '15','16','17', '18', '19','20','21','22']
#     list_movie = pandas.read_csv(text, sep='|', names=r_cols, encoding='latin-1')
#     list_year_movie = list_movie['year'].values
#     return list_year_movie


# def get_dataframe_movies_csv(text):
#     """
#     đọc file csv của movilens, lưu thành dataframe với 3 cột user id, title, genres
#     """
#     movie_cols = ['movie_id', 'title', 'genres']
#     movies = pandas.read_csv(text, sep=',', names=movie_cols, encoding='latin-1')
#     return movies
