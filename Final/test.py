import json
import pandas
if __name__ == "__main__":
    # userid = 1
    # list = ['a','b','c']
    # dic = {'userid':1, 're':list}
    # json_string1 = json.dumps(dic, separators=(',',':'))
    # print(json_string1)
    # print(type(json_string1))
    # # print(json.dumps(list, separators=(',')))

    # json_string2 = '{"language": "python", "author": "Rossum"}'
    # python_language = json.loads(json_string2)
    # print(type(python_language))
    data = pandas.read_csv('dataset/item.csv')
    print(data)
    a = data.loc[data.user_id == "user1"]
    print(a.empty)
    b = a.loc[data.movie_id == "tt21544"]
    print(b.empty)
    if b.empty or a.empty:
        print("A")
    else: print("B")