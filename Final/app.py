from datetime import time
from fastapi import FastAPI #import class FastAPI() từ thư viện fastapi
import collab_filtering
import read_data_function
import pandas
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

class Rate(BaseModel): # kế thừa từ class Basemodel và khai báo các biến
    userid: str
    movieid: str
    rate: float

app = FastAPI() # gọi constructor và gán vào biến app
origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/rating/")
async def create_item(rate: Rate): # khai báo dưới dạng parameter
    print(rate)
    data = pandas.read_csv('dataset/data.csv')
    a = data.loc[data.user_id == rate.userid]
    b = a.loc[data.movie_id == rate.movieid]
    if a.empty or b.empty:
        input = {'user_id':rate.userid,'movie_id':rate.movieid,'rating':rate.rate}
        data = data.append(input, ignore_index=True)
    else:
        data.loc[b.index.values] = [rate.userid,rate.movieid,rate.rate]
    
    data.to_csv('dataset/data.csv',sep=',', index = False, encoding='utf-8')
    
    return rate


@app.get("/") # giống flask, khai báo phương thức get và url
async def root(): # do dùng ASGI nên ở đây thêm async, nếu bên thứ 3 không hỗ trợ thì bỏ async đi
    return {"message": "Hello World"}

@app.get("/list")
async def lists():
    userid = 1
    list = [1,2,3,4]
    dic = {'userid':userid, 're':list}
    # return  json.dumps(dic, separators=(',',':'))
    return dic

@app.get("/user/{user_id}")
async def recommend(user_id: str):
    # data_matrix, userid, idmovie = read_data_function.get_dataframe_ratings_base('dataset/ml-100k/ub.base')
    data_matrix, userid, idmovie = read_data_function.get_dataframe_ratings()
    cf_rs = collab_filtering.CF(data_matrix, k=2, uuCF=1)
    cf_rs.fit()
    result = []
    # print(userid)
    # print(idmovie)
    try:
        userrecommen = userid[user_id]
        if(len(userid) > 2 and len(idmovie) > 1):
            lists = cf_rs.recommend_top(userrecommen)
            for i in lists:
                result.append(idmovie[i])
        print(result)
        return {'list':result}
    except:
        print("error")
        return {'list':result}

    
    

# uvicorn app:app --host 127.0.0.1 --port 8000
# uvicorn app:app --host 192.168.1.80 --port 8000
# uvicorn <filename>:<app-name> --reload
