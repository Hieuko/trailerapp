package com.example.trailerapp.presenter.favourite.watch

import com.example.trailerapp.data.remote.model.MovieInformation
import com.google.firebase.auth.FirebaseUser

interface WatchContract {
    interface View{
        fun updateViewData(movie: MovieInformation)
    }

    interface Presenter{
        fun getUserCurrent(): FirebaseUser?
        fun getWatchMovieList()
        fun removeReadWatchedList()
    }
}