package com.example.trailerapp.presenter.trailer

import com.example.trailerapp.data.repository.MovieRepository
import kotlinx.coroutines.*

class YoutubeAPIPresenter(private val view: YoutubeAPIContract.View): YoutubeAPIContract.Presenter {
    private var jobYoutube = Job()
    private val handler = CoroutineExceptionHandler { coroutineContext, throwable ->

    }
    private val scopeMainSeach = CoroutineScope(jobYoutube + Dispatchers.Main + handler)
    private val scopeIOSeach = CoroutineScope(jobYoutube + Dispatchers.IO + handler)

    private val repository = MovieRepository()

    override fun getVideoTrailer(movieId: String) {
        scopeIOSeach.launch {
            val dataList = repository.callVideoTrailer(movieId)
            scopeMainSeach.launch() {
                view.updateViewData(dataList!!)
            }
        }
    }
}
