package com.example.trailerapp.presenter.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.trailerapp.R
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.presenter.BaseFragment
import com.example.trailerapp.presenter.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment(), LoginContract.View {
    private lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = LoginPresenter(this, FirebaseRepository(requireActivity()))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProgressBar(this.login_fragment_progress)
        hideKeyboard(requireView())


        login_fragment_btn_login.setOnClickListener {
            var email: String = login_fragment_edt_email.text.toString()
            var password: String = login_fragment_edt_password.text.toString()
            showProgressBar()
            presenter.checkLogin(email, password)
        }

        login_fragment_btn_register.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    override fun updateUI(message: String) {
        hideProgressBar()
        when(message){
            "e_null" -> {
                login_fragment_edt_email.error = "Require Email"
            }
            "e_invalid" -> {
                login_fragment_edt_email.error = "Email format abc@gmail"
            }
            "p_null" -> {
                login_fragment_edt_password.error = "Require Password"
            }
            "p_invalid" -> {
                login_fragment_edt_password.error = "6 char password required"
            }
            "success" -> {
                val intent = Intent(activity, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
            "failure" -> Toast.makeText(context, "Sai email hoac password", Toast.LENGTH_LONG)
                .show()
        }

    }

    override fun onStart() {
        super.onStart()
        var firebaseUser = presenter.getUserCurrent()
        if(firebaseUser != null){
            val intent = Intent(activity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

}