package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.TopMovie
import kotlinx.android.synthetic.main.layout_top_movie.view.*

class TopMovieAdapter(var onClick: OnClickTopMovie) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_ITEM = 1
    private val TYPE_LOADING = 2
    private var isLoadingAdd: Boolean = false

    private var dataList = ArrayList<TopMovie>()

    fun setData(data: ArrayList<TopMovie>) {
        dataList = data
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        if (dataList != null && position == dataList.size - 1 && isLoadingAdd) return TYPE_LOADING
        return TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_top_movie, parent, false)
            return TopMovieViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.layout_loading, parent, false)
            return LoadingViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == TYPE_ITEM) {
            val m = dataList.get(position)
            val topMovieViewHolder = holder as TopMovieViewHolder
            holder.onBindData(m)
            holder.itemView.setOnClickListener(
                View.OnClickListener {
                    onClick.clickOnItem(m)
                }
            )
        }
    }

    override fun getItemCount(): Int {
        if (dataList == null) return 0
        return dataList.size
    }

    class TopMovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBindData(m: TopMovie) {
            itemView.txt_film.text = m.title
            Glide.with(itemView.context).load(m.image).placeholder(R.drawable.load_image)
                .into(itemView.image_popular)
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    fun addFooterLoading() {
        isLoadingAdd = true
        dataList.add(TopMovie("", "", "", "", ""))
        notifyDataSetChanged()
    }

    fun removeFooterLoading() {
        isLoadingAdd = false
        var position = dataList.size - 1
        val topMovie = dataList.get(position)
        if (topMovie != null) {
            dataList.removeAt(position)
            notifyItemRemoved(position)
        }
    }
}