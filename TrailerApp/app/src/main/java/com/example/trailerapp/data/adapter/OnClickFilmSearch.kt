package com.example.trailerapp.data.adapter

import com.example.trailerapp.data.remote.model.FilmSearch

interface OnClickFilmSearch {
    fun onClickItem(film: FilmSearch)
}