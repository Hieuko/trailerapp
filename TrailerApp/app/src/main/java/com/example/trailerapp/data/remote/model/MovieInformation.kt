package com.example.trailerapp.data.remote.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class MovieInformation(var userid:String? = null, var movieid:String? = null, var title:String? = null, var img: String? = null, var time: Long? = null) {
    @Exclude
    fun toMap(): Map<String, Any?>{
        return mapOf(
            "userid" to userid,
            "movieid" to movieid,
            "title" to title,
            "img" to img,
            "time" to time,
        )
    }
}