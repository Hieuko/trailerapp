package com.example.trailerapp.data.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class TabPagerApdapter(val listFragment: ArrayList<Fragment>, fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    override fun getCount(): Int {
        return listFragment.size
    }

    override fun getItem(position: Int): Fragment {
        return listFragment.get(position)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        when(position){
            0 -> title = "Favourite"
            1 -> title = "Watch"
        }
        return title
    }
}