package com.example.trailerapp.presenter.home

import com.example.trailerapp.data.remote.model.DetailMovie
import com.example.trailerapp.data.remote.model.TopMovie
import com.google.firebase.auth.FirebaseUser

interface HomeContract {
    interface View{
        fun updateViewDataMostPopularMovie(dataList: ArrayList<TopMovie>)
        fun updateViewDataNewFilm(dataList: ArrayList<TopMovie>)
        fun updateViewDataRecommend(dataList: ArrayList<DetailMovie>?)
    }

    interface Presenter{
        fun getUserCurrent(): FirebaseUser?
        fun getMostPopularMovieApi()
        fun getMostPopularMovie()
        fun getNewFilmApi()
        fun getNewFilm()
        fun getRecommendApi()
        fun cleanup()
    }
}