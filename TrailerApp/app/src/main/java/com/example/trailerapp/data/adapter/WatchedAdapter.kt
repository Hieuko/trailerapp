package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.MovieInformation
import kotlinx.android.synthetic.main.layout_watched.view.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class WatchedAdapter(val onclick:OnClickMovie):
    RecyclerView.Adapter<WatchedAdapter.WatchViewHolder>() {
    private var dataList = ArrayList<MovieInformation>()
    fun setDataList(list :ArrayList<MovieInformation>){
        this.dataList=list
    }
    fun addData(movie: MovieInformation){
        val index = checkMovie(movie)
        if (checkMovie(movie) == -1){
            dataList.add(movie)
        } else{
            dataList.removeAt(index)
            dataList.add(movie)
        }
        Collections.sort(dataList, object :Comparator<MovieInformation>{
            override fun compare(p0: MovieInformation?, p1: MovieInformation?): Int {
                if (p0!!.time!! > p1!!.time!!) return -1
                else if (p0!!.time!! == p1!!.time!!) return 0
                else return 1
            }
        })
        notifyDataSetChanged()
    }
    private fun checkMovie(movie: MovieInformation): Int{
        if (!dataList.isEmpty()){
            for (i in 0..(dataList.size-1)){
                if (dataList.get(i).movieid == movie.movieid){
                    return i
                }
            }
        }
        return -1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WatchViewHolder {
        return WatchViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_watched, parent, false)
        )
    }

    override fun onBindViewHolder(holder: WatchViewHolder, position: Int) {
        val m = dataList.get(position)
        holder.onBindData(m)
        holder.itemView.setOnClickListener(
            View.OnClickListener {
                onclick.onClick(m)
            }
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class WatchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBindData(m: MovieInformation) {
            itemView.watched_layout_title.text = m.title
            Glide.with(itemView.context).load(m.img).into(itemView.watched_layout_img)
        }
    }
}