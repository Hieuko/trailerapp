package com.example.trailerapp.presenter.profile

import android.net.Uri
import com.example.trailerapp.data.remote.model.User
import com.google.firebase.auth.FirebaseUser

interface ProfileContract {
    interface View{
        fun updateViewData(user: User)
    }
    interface Presenter{
        fun readUser()
        fun logout()
        fun removeReadUser()
        fun updateUser(user: User)
        fun getUserCurrent(): FirebaseUser?
        fun uploadImage(imageURI: Uri?, ex: String?)
    }
    interface OnProfileListener{
        fun updateUserListen(user: User)
    }
}