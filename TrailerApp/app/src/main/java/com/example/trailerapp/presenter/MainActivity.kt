package com.example.trailerapp.presenter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.trailerapp.R
import com.example.trailerapp.data.adapter.ViewPagerAdapter
import com.example.trailerapp.presenter.favourite.InformationFragment
import com.example.trailerapp.presenter.home.HomeFragment
import com.example.trailerapp.presenter.profile.ProfileFragment
import com.example.trailerapp.presenter.search.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private var listFragment = ArrayList<Fragment>()
//    private val REQUEST_RECORD_AUDIO_PERMISSION_RESULT = 100


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        requestRecordAudioPermission()
        initData()
        initAction()
    }

    private fun initData() {
        listFragment =
            arrayListOf(HomeFragment(), SearchFragment(), InformationFragment(), ProfileFragment())
        val viewPagerAdapter = ViewPagerAdapter(listFragment, supportFragmentManager)
        main_activity_viewPager.adapter = viewPagerAdapter
    }

    private fun initAction() {
        main_activity_bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.ic_movie -> {
                    main_activity_viewPager.currentItem = 0
                }
                R.id.ic_search -> {
                    main_activity_viewPager.currentItem = 1
                }
                R.id.ic_infor -> {
                    main_activity_viewPager.currentItem = 2
                }
                R.id.ic_profile -> {
                    main_activity_viewPager.currentItem = 3
                }
            }
            true
        }

        main_activity_viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        main_activity_bottom_navigation.menu.findItem(R.id.ic_movie)
                            .setChecked(true)
                    }
                    1 -> {
                        main_activity_bottom_navigation.menu.findItem(R.id.ic_search)
                            .setChecked(true)
                    }
                    2 -> {
                        main_activity_bottom_navigation.menu.findItem(R.id.ic_infor)
                            .setChecked(true)
                    }
                    3 -> {
                        main_activity_bottom_navigation.menu.findItem(R.id.ic_profile)
                            .setChecked(true)
                    }
                }
            }
        })

//        main_activity_viewPager.setCurrentItem(1)
    }

//    private fun requestRecordAudioPermission() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) ==
//                PackageManager.PERMISSION_GRANTED
//            ) {
//                initData()
//                initAction()
//            } else {
//                if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
////                    Toast.makeText(this,
////                        "App required access to audio", Toast.LENGTH_SHORT).show();
//                }
//                requestPermissions(
//                    arrayOf(Manifest.permission.RECORD_AUDIO),
//                    REQUEST_RECORD_AUDIO_PERMISSION_RESULT
//                )
//            }
//        }
//
//    }
//
//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//
//        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION_RESULT && permissions[0] == Manifest.permission.RECORD_AUDIO) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Log.d(
//                    "KO",
//                    "Manifest.permission.RECORD_AUDIO success"
//                )
//                initData()
//                initAction()
//
//            } else {
//                Log.d("KO", "Application will not have audio on record")
//            }
//        }
//    }

}