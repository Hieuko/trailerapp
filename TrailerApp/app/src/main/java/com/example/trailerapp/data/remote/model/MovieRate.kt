package com.example.trailerapp.data.remote.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@IgnoreExtraProperties
data class MovieRate(
    @Expose
    @SerializedName("userid")
    var userid:String? = null,
    @Expose
    @SerializedName("movieid")
    var movieid:String? = null,
    @Expose
    @SerializedName("rate")
    var rate: Float? = null) {

    var time: Long? = null
    @Exclude
    fun toMap(): Map<String, Any?>{
        return mapOf(
            "userid" to userid,
            "movieid" to movieid,
            "rate" to rate,
            "time" to time,
        )
    }
}