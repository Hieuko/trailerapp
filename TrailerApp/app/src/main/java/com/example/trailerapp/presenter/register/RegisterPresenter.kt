package com.example.trailerapp.presenter.register

import com.example.trailerapp.data.remote.model.User
import com.example.trailerapp.data.repository.FirebaseRepository

class RegisterPresenter(val view: RegisterContract.View, var repository: FirebaseRepository) :
    RegisterContract.Presenter, RegisterContract.OnRegisterListener {
    override fun register(email: String, password: String, confirmPassword: String, username: String
    ) {
        if(validateForm(email, password, confirmPassword, username)){
            repository.register(email, password, username, this)
        }
    }

    override fun onSuccess(message: String?) {
        view.updateUI(message!!)
    }

    override fun onFailure(message: String?) {
        view.updateUI(message!!)
    }

    private fun validateForm(email: String, password: String, confirmPassword: String, username: String
    ): Boolean {
        var valid = true
        if (email.isNullOrEmpty()) {
            view.updateUI("e_null")
            valid = false
        }
        if (!email.contains("@")) {
            view.updateUI("e_invalid")
            valid = false
        }
        if (password.isNullOrEmpty()) {
            view.updateUI("p_null")
            valid = false
        }
        if (password.length < 6) {
            view.updateUI("p_invalid")
            valid = false
        }
        if (confirmPassword.isNullOrEmpty()){
            view.updateUI("pc_null")
            valid = false
        } else if (!password.equals(confirmPassword)){
            view.updateUI("pass_not_confirm")
            valid = false
        }
        if (username.isNullOrEmpty()){
            view.updateUI("username_null")
            valid = false
        }
        return valid
    }


}