package com.example.trailerapp.data.repository

import android.app.Activity
import android.net.Uri
import android.util.Log
import com.example.trailerapp.data.adapter.OnMovieListener
import com.example.trailerapp.data.remote.model.MovieInformation
import com.example.trailerapp.data.remote.model.MovieRate
import com.example.trailerapp.data.remote.model.User
import com.example.trailerapp.presenter.detail.DetailContract
import com.example.trailerapp.presenter.login.LoginContract
import com.example.trailerapp.presenter.profile.ProfileContract
import com.example.trailerapp.presenter.register.RegisterContract
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.tasks.await
import java.util.*


class FirebaseRepository(val activity: Activity) {
    private val TAG = "KO"
    private var userCurrent: FirebaseUser? = null
    private var firebaseAuth: FirebaseAuth = Firebase.auth

    private var databaseReference: DatabaseReference = Firebase.database.reference
    private lateinit var userDatabaseReference: DatabaseReference
    private var storageRef: StorageReference = Firebase.storage.reference

    private var userDetailListener: ValueEventListener? = null
    private var movieWatchedsListener: ChildEventListener? = null
    private var movieFavouritesListener: ChildEventListener? = null

    fun signIn(email: String, password: String, login: LoginContract.OnLoginListener) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    userCurrent = checkUser()
                    login.onSuccess("success")
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d(TAG, "signInWithEmail:failure", task.exception)
                    login.onFailure("failure")
                }
            }
    }
    fun logout() {
        Log.d(TAG, "logout() called")
        firebaseAuth.signOut()
    }
    fun register(email: String, password: String, username: String, register: RegisterContract.OnRegisterListener) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    var userId: String = checkUser()?.uid!!
                    var user: User = User(userId, email, username)
                    storeUser(user)
                    logout()
                    Log.d(TAG, "register() called success")
                    register.onSuccess("success");
                } else {
                    Log.d(TAG, "register() called failure", it.exception)
                    register.onFailure("failure");
                }
            }
    }
    fun storeUser(user: User) {
        databaseReference.child("Users").child(userCurrent!!.uid).setValue(user.toMap())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "storeUser() called succeess $user")
                } else {
                    Log.d(TAG, "storeUser() called failure ${it.exception}")
                }
            }
    }
    fun readUserDetail(profileListener: ProfileContract.OnProfileListener) {
        userDatabaseReference = databaseReference.child("Users").child(userCurrent!!.uid)
        Log.d(TAG, "readUserDetail() called with: ${userCurrent!!.uid}")
        // read / listen
        userDetailListener = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue<User>()
                Log.d(TAG, "User Detail ${user.toString()}")
                profileListener.updateUserListen(user!!)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d(TAG, "readUserDetail called with: error = $error")
            }
        }
        userDatabaseReference.addValueEventListener(userDetailListener!!)
    }
    fun removeUserDetail(){
        if(userDetailListener!=null)
            userDatabaseReference.removeEventListener(userDetailListener!!)
    }
    fun storageImage(imageURI: Uri?, ex: String?){
        Log.d(TAG, "storageImage() called with: imageURI = $imageURI, ex = $ex")
        val fileRererence = storageRef.child(userCurrent!!.uid).child("${System.currentTimeMillis()}.$ex")
        fileRererence.putFile(imageURI!!)
            .continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                fileRererence.downloadUrl
            }
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    val downloadUri = task.result
                    val url: String = downloadUri.toString()
                    Log.d(TAG, "storageImage() called with: url = $url")

                    val map = HashMap<String, Any>()
                    map["imgURL"] = url
                    userDatabaseReference.updateChildren(map)
                } else {
                    Log.d(TAG, "storageData() called with: task = ${task.exception}")
                }
            }
    }
    fun checkUser(): FirebaseUser? {
        if (userCurrent == null) {
            userCurrent = firebaseAuth.currentUser
        }
        return userCurrent
    }
    fun readMovieWatched(movieid: String, onMovieListener: OnMovieListener){
        val readWatched = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val movie = snapshot.getValue<MovieInformation>()
                onMovieListener.onMovieWatchListener(movie)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d(TAG, "readMovieWatched called with: error = $error")
            }
        }
        databaseReference.child("Movies").child("Watcheds").child(userCurrent!!.uid).child(movieid).addListenerForSingleValueEvent(readWatched)
    }
    fun readMovieWatcheds(onMovieListener: OnMovieListener){
        movieWatchedsListener = object : ChildEventListener{
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val movie = snapshot.getValue<MovieInformation>()
                onMovieListener.onMovieWatchListener(movie)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val movie = snapshot.getValue<MovieInformation>()
                onMovieListener.onMovieWatchChangedListener(movie)
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        }
        databaseReference.child("Movies").child("Watcheds").child(userCurrent!!.uid).addChildEventListener(movieWatchedsListener!!)
    }
    fun removeMovieWatcheds(){
        if(movieWatchedsListener != null){
            databaseReference.child("Movies").child("Watcheds").child(userCurrent!!.uid).removeEventListener(movieWatchedsListener!!)
        }
    }
    fun storeMovieWatched(movie: MovieInformation){
        databaseReference.child("Movies").child("Watcheds").child(userCurrent!!.uid).child(movie.movieid!!).setValue(movie.toMap())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "storeMovieWatched() called succeess $movie")
                } else {
                    Log.d(TAG, "storeMovieWatched() called failure ${it.exception}")
                }
            }
    }
    fun readMovieFavourite(movieid: String, onMovieListener: OnMovieListener){
        val readFavourite = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val movie = snapshot.getValue<MovieInformation>()
                onMovieListener.onMovieFavoriteListener(movie)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d(TAG, "readMovieFavourite called with: error = $error")
            }
        }
        databaseReference.child("Movies").child("Favourites").child(userCurrent!!.uid).child(movieid).addListenerForSingleValueEvent(readFavourite)
    }
    fun readMovieFavourites(onMovieListener: OnMovieListener){
        movieFavouritesListener = object : ChildEventListener{
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val movie = snapshot.getValue<MovieInformation>()
                onMovieListener.onMovieFavoriteListener(movie)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                val movie = snapshot.getValue<MovieInformation>()
                onMovieListener.onMovieFavoriteRemovedListener(movie)
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        }
        databaseReference.child("Movies").child("Favourites").child(userCurrent!!.uid).addChildEventListener(movieFavouritesListener!!)
    }
    fun removeMovieFavourites(){
        if(movieFavouritesListener != null){
            databaseReference.child("Movies").child("Favourites").child(userCurrent!!.uid).removeEventListener(movieFavouritesListener!!)
        }
    }
    fun storeMovieFavourite(movie: MovieInformation?, movieid: String){
        databaseReference.child("Movies").child("Favourites").child(userCurrent!!.uid).child(movieid).setValue(movie?.toMap())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "storeMovieFavourite() called succeess $movie")
                } else {
                    Log.d(TAG, "storeMovieFavourite() called failure ${it.exception}")
                }
            }
    }
    fun readMovieRate(movieid: String, onMovieListener: OnMovieListener){
        val readRate = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val movie = snapshot.getValue<MovieRate>()
                onMovieListener.onMovieRateListener(movie)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d(TAG, "readMovieRate called with: error = $error")
            }
        }
        databaseReference.child("Movies").child("Rates").child(userCurrent!!.uid).child(movieid).addListenerForSingleValueEvent(readRate)
    }
    fun storeMovieRate(movie: MovieRate){
        databaseReference.child("Movies").child("Rates")
            .child(userCurrent!!.uid).child(movie.movieid!!).setValue(movie.toMap())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "storeMovieRate() called succeess $movie")
                } else {
                    Log.d(TAG, "storeMovieRate() called failure ${it.exception}")
                }
            }
    }

    suspend fun signIn2(email: String, password: String): FirebaseUser? {
        firebaseAuth.signInWithEmailAndPassword(email, password).await()
        return firebaseAuth.currentUser ?: throw FirebaseAuthException("", "")
    }
}