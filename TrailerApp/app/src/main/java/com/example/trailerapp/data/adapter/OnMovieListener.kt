package com.example.trailerapp.data.adapter

import com.example.trailerapp.data.remote.model.MovieInformation
import com.example.trailerapp.data.remote.model.MovieRate

interface OnMovieListener{
    fun onMovieWatchListener(movie: MovieInformation?)
    fun onMovieWatchChangedListener(movie: MovieInformation?)
    fun onMovieFavoriteListener(movie: MovieInformation?)
    fun onMovieFavoriteRemovedListener(movie: MovieInformation?)
    fun onMovieRateListener(movie: MovieRate?)
}