package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.TopMovie
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.layout_item_slide.view.*
import kotlinx.android.synthetic.main.layout_top_movie.view.*

class SliderAdapter(var onClick: OnClickTopMovie): SliderViewAdapter<SliderAdapter.SliderAdapterVH>() {
    private var mSliderItems = ArrayList<TopMovie>()

    fun setData(data: ArrayList<TopMovie>) {
        mSliderItems = data
        notifyDataSetChanged()
    }

    class SliderAdapterVH(itemView: View): SliderViewAdapter.ViewHolder(itemView) {
        fun onBindData(m: TopMovie){
            Glide.with(itemView.context).load(m.image).placeholder(R.drawable.load_image)
                .into(itemView.item_slide_layout_img)
        }
    }

    override fun getCount(): Int {
        if (mSliderItems == null) return 0
        return mSliderItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?): SliderAdapterVH {
        val view = LayoutInflater.from(parent?.context)
            .inflate(R.layout.layout_item_slide, parent, false)
        return SliderAdapterVH(view)
    }

    override fun onBindViewHolder(holder: SliderAdapterVH?, position: Int) {
        val m = mSliderItems.get(position)
        holder?.onBindData(m)
        holder?.itemView?.setOnClickListener(
            View.OnClickListener {
                onClick.clickOnItem(m)
            }
        )
    }
}

