package com.example.trailerapp.presenter.search

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trailerapp.R
import com.example.trailerapp.data.adapter.*
import com.example.trailerapp.data.remote.model.DetailMovie
import com.example.trailerapp.data.remote.model.FilmSearch
import com.example.trailerapp.data.remote.model.TopMovie
import com.example.trailerapp.presenter.detail.DetailActivity
import com.example.trailerapp.presenter.home.PaginationScrollListener
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.*


class SearchFragment : Fragment(), OnClickFilmSearch, SearchContract.View, OnClickTopMovie {
    private val TAG = "KO Search"
    private lateinit var presenter: SearchContract.Presenter
    private lateinit var searchadapter: SearchAdapter
    private lateinit var noadapter: NoSearchAdapter

    private var isLoadingSearch = false
    private var isLastPageSearch = false
    private var totalPageSearch = 0
    private var currentPageSearch = 1
    private var searchlist = ArrayList<FilmSearch>()

    private var isLoadingNo = false
    private var isLastPageNo = false
    private var totalPageNo = 0
    private var currentPageNo = 1
    private var noList = ArrayList<TopMovie>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchadapter = SearchAdapter(this)
        noadapter = NoSearchAdapter(this)
        presenter = SearchPresenter(this)
        Log.d("KO", "Search onCreate() called")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.d(
            "KO",
            "Search onCreateView() called"
        )
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val itemDecoration =
            DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        search_fragment_rc_search.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        search_fragment_rc_search.addItemDecoration(itemDecoration)

        search_fragment_search?.setSubmitButtonEnabled(true)

        search_fragment_search?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d("KO Search", "onQueryTextSubmit() called with: query = $query")
                if (query?.isNotEmpty() == true) {
                    presenter.getSearchMovie(query)
                } else {
                    presenter.getNoSearchMovie()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d("KO Search", "onQueryTextChange() called with: newText = $newText")
                if (newText?.isNotEmpty() == true) {
                    Log.d(TAG, "onQueryTextChange() called with: newText = $newText")
                    presenter.getSearchMovie(newText)
                } else {
                    presenter.getNoSearchMovie()
                }
                return false
            }
        })
        presenter.getNoSearchMovie()
    }

    override fun updateViewDataSearch(dataList: ArrayList<FilmSearch>) {
        Log.d("KO Search", "updateViewDataSearch() called with: dataList = $dataList")
        search_fragment_rc_search.adapter = searchadapter
        if (!dataList.isNullOrEmpty()) {
            val size = dataList.size
            totalPageSearch = if (size % 10 == 0) size / 10 else size / 10 + 1
            val linearLayoutManager = search_fragment_rc_search.layoutManager as LinearLayoutManager
            setFistDataSearch(dataList)
            search_fragment_rc_search.addOnScrollListener(object :
                PaginationScrollListener(linearLayoutManager!!) {
                override fun loadMoreItems() {
                    isLoadingSearch = true
                    currentPageSearch += 1
                    loadNextPageSearch(dataList)
                }

                override fun isLoading(): Boolean {
                    return isLoadingSearch
                }

                override fun isLastPage(): Boolean {
                    return isLastPageSearch
                }
            })
        }
    }

    private fun loadNextPageSearch(dataList: ArrayList<FilmSearch>) {
        CoroutineScope(Job() + Dispatchers.Main).launch {
            delay(1000)
            var arrayList = getDataFilm(dataList, totalPageNo, currentPageNo)

            searchadapter.removeFooterLoading()
            searchlist.addAll(arrayList)
            searchadapter.notifyDataSetChanged()

            isLoadingSearch = false
            if (currentPageSearch < totalPageSearch) {
                searchadapter.addFooterLoading()
            } else {
                isLastPageSearch = true
            }
        }
    }

    private fun setFistDataSearch(dataList: ArrayList<FilmSearch>) {
        searchlist.addAll(getDataFilm(dataList, totalPageSearch, currentPageSearch))
        Log.d(TAG, "setFistDataSearch() called with: dataList = $searchlist")
        searchadapter.setData(searchlist)
        if (currentPageSearch < totalPageSearch) {
            searchadapter.addFooterLoading()
        } else {
            isLastPageSearch = true
        }
    }

    override fun updateViewDataNoSearch(dataList: ArrayList<TopMovie>) {
        Log.d("KO Search", "updateViewDataNoSearch() called with: dataList = $dataList")
        search_fragment_rc_search.adapter = noadapter
        if (!dataList.isNullOrEmpty()) {
            val size = dataList.size
            totalPageNo = if (size % 10 == 0) size / 10 else size / 10 + 1
            val linearLayoutManager = search_fragment_rc_search.layoutManager as LinearLayoutManager
            setFistDataNo(dataList)
            search_fragment_rc_search.addOnScrollListener(object :
                PaginationScrollListener(linearLayoutManager!!) {
                override fun loadMoreItems() {
                    isLoadingNo = true
                    currentPageNo += 1
                    loadNextPageNo(dataList)
                }

                override fun isLoading(): Boolean {
                    return isLoadingNo
                }

                override fun isLastPage(): Boolean {
                    return isLastPageNo
                }
            })
        }
    }

    private fun loadNextPageNo(dataList: ArrayList<TopMovie>) {
        CoroutineScope(Job() + Dispatchers.Main).launch {
            delay(1000)
            var arrayList = getData(dataList, totalPageNo, currentPageNo)

            noadapter.removeFooterLoading()
            noList.addAll(arrayList)
            noadapter.notifyDataSetChanged()

            isLoadingNo = false
            if (currentPageNo < totalPageNo) {
                noadapter.addFooterLoading()
            } else {
                isLastPageNo = true
            }
        }
    }

    private fun setFistDataNo(dataList: ArrayList<TopMovie>) {
        noList.addAll(getData(dataList, totalPageNo, currentPageNo))
        noadapter.setData(noList)
        if (currentPageNo < totalPageNo) {
            noadapter.addFooterLoading()
        } else {
            isLastPageNo = true
        }
    }

    private fun getData(data: ArrayList<TopMovie>, totalPage: Int, currentPage: Int): ArrayList<TopMovie> {
        var result = ArrayList<TopMovie>()
        if (totalPage == 1){
            for(i in 0..(data.size-1))
                result.add(data.get(i))
        } else if (currentPage == 1 && totalPage > 1) {
            for (i in 0..9) {
                result.add(data.get(i))
            }
        } else if (currentPage > 1 && currentPage < totalPage){
            for(i in ((currentPage-1)*10)..(currentPage*10-1)){
                result.add(data.get(i))
            }
        } else if (currentPage == totalPage){
            for(i in ((currentPage-1)*10)..(data.size-1)){
                result.add(data.get(i))
            }
        }
        return result
    }

    private fun getDataFilm(data: ArrayList<FilmSearch>, totalPage: Int, currentPage: Int): ArrayList<FilmSearch> {
        var result = ArrayList<FilmSearch>()
        if (totalPage == 1){
            for(i in 0..(data.size-1))
                result.add(data.get(i))
        } else if (currentPage == 1 && totalPage > 1) {
            for (i in 0..9) {
                result.add(data.get(i))
            }
        } else if (currentPage > 1 && currentPage < totalPage){
            for(i in ((currentPage-1)*10)..(currentPage*10-1)){
                result.add(data.get(i))
            }
        } else if (currentPage == totalPage){
            for(i in ((currentPage-1)*10)..(data.size-1)){
                result.add(data.get(i))
            }
        }
        return result
    }

    override fun onClickItem(film: FilmSearch) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("movieid", film.id)
        startActivity(intent)
    }

    override fun clickOnItem(m: TopMovie) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("movieid", m.id)
        startActivity(intent)
    }

    override fun clickOnItem(m: DetailMovie) {
        TODO("Not yet implemented")
    }

    override fun onDestroyView() {
        Log.d("KO", "Search onDestroyView() called")
        super.onDestroyView()
    }
    override fun onDestroy() {
        Log.d("KO", "Search onDestroy() called")
        super.onDestroy()
    }

    override fun onPause() {
        Log.d("KO", "Search onPause() called")
        super.onPause()
    }

    override fun onResume() {
        Log.d("KO", "Search onResume() called")
        super.onResume()
    }

    override fun onStart() {
        Log.d("KO", "Search onStart() called")
        super.onStart()
    }

    override fun onStop() {
        Log.d("KO", "Search onStop() called")
        super.onStop()
    }
}