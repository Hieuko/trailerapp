package com.example.trailerapp.data.remote.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable

@IgnoreExtraProperties
data class User(var id: String? = null, var email: String? = null, var username: String? = null, var fullname: String? = "", var imgURL: String? = "default", var country: String? = "") :
    Serializable {

    @Exclude
    fun toMap(): Map<String, Any?>{
        return mapOf(
            "id" to id,
            "email" to email,
            "username" to username,
            "fullname" to fullname,
            "imgURL" to imgURL,
            "country" to country
        )
    }
}