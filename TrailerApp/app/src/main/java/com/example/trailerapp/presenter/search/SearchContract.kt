package com.example.trailerapp.presenter.search

import com.example.trailerapp.data.remote.model.FilmSearch
import com.example.trailerapp.data.remote.model.TopMovie

interface SearchContract {
    interface View{
        fun updateViewDataSearch(dataList: ArrayList<FilmSearch>)
        fun updateViewDataNoSearch(dataList: ArrayList<TopMovie>)
    }

    interface Presenter{
        fun getSearchMovie(search: String)
        fun getNoSearchMovieApi()
        fun getNoSearchMovie()
        fun cancelJob()
    }
}