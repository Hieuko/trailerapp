package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.TopMovie
import kotlinx.android.synthetic.main.layout_movie_item.view.*

class NoSearchAdapter(
    var onClick: OnClickTopMovie
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var dataList = ArrayList<TopMovie>()
    private val TYPE_ITEM = 1
    private val TYPE_LOADING = 2
    private var isLoadingAdd: Boolean = false

    fun setData(list: ArrayList<TopMovie>) {
        this.dataList = list
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        if (dataList != null && position == dataList.size - 1 && isLoadingAdd) return TYPE_LOADING
        return TYPE_ITEM
    }

    inner class NoSearchViewHolder(var itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun OnBinData(film: TopMovie) {
            itemView.movie_item_layout_title.text = film.title
            itemView.movie_item_layout_year.text = film.year
            Glide.with(itemView.context).load(film.image).into(itemView.movie_item_layout_img)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_movie_item, parent, false)
            return NoSearchViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.layout_loading_vertical, parent, false)
            return LoadingViewHolder(view)
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == TYPE_ITEM) {
            val film_item: TopMovie = dataList.get(position)
            val noSearchViewHolder = holder as NoSearchViewHolder
            noSearchViewHolder.itemview.setOnClickListener() { onClick.clickOnItem(film_item) }
            return noSearchViewHolder.OnBinData(film_item)
        }
    }

    override fun getItemCount(): Int {
        if (dataList == null) return 0
        return dataList.size
    }

    fun addFooterLoading() {
        isLoadingAdd = true
        dataList.add(TopMovie("", "", "", "", ""))
        notifyDataSetChanged()
    }

    fun removeFooterLoading() {
        isLoadingAdd = false
        var position = dataList.size - 1
        val topMovie = dataList.get(position)
        if (topMovie != null) {
            dataList.removeAt(position)
            notifyItemRemoved(position)
        }
    }
}