package com.example.trailerapp.presenter.login

import com.example.trailerapp.data.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class LoginPresenter(val view: LoginContract.View, var repository : FirebaseRepository): LoginContract.Presenter, LoginContract.OnLoginListener {
    var firebaseUser: FirebaseUser? = null

    override fun checkLogin(email: String, password: String) {
        if (validateForm(email, password))
            repository.signIn(email, password, this)
    }

    private fun validateForm(email: String, password: String): Boolean {
        var valid = true
        if (email.isNullOrEmpty()){
            view.updateUI("e_null")
            valid = false
        }
        if(!email.contains("@")){
            view.updateUI("e_invalid")
            valid = false
        }
        if (password.isNullOrEmpty()){
            view.updateUI("p_null")
            valid = false
        }
        if (password.length < 6){
            view.updateUI("p_invalid")
            valid = false
        }
        return valid
    }

    override fun getUserCurrent(): FirebaseUser? {
        return repository.checkUser()
    }

    override fun onSuccess(message: String?) {
        view.updateUI(message!!)
    }

    override fun onFailure(message: String?) {
        view.updateUI(message!!)
    }
}