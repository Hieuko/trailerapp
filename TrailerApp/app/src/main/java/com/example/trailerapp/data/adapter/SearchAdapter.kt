package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.FilmSearch
import kotlinx.android.synthetic.main.layout_movie_item.view.*

class SearchAdapter(
    var onClick: OnClickFilmSearch
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var dataList = ArrayList<FilmSearch>()
    private val TYPE_ITEM = 1
    private val TYPE_LOADING = 2
    private var isLoadingAdd: Boolean = false

    fun setData(list: ArrayList<FilmSearch>) {
        this.dataList = list
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        if (dataList != null && position == dataList.size - 1 && isLoadingAdd) return TYPE_LOADING
        return TYPE_ITEM
    }

    inner class SearchViewHolder(var itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun onBindData(film: FilmSearch) {
            itemView.movie_item_layout_title.text = film.title
            itemView.movie_item_layout_year.text = film.description
            Glide.with(itemView.context).load(film.image).into(itemView.movie_item_layout_img)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_movie_item, parent, false)
            return SearchViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.layout_loading_vertical, parent, false)
            return LoadingViewHolder(view)
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == TYPE_ITEM) {
            val film_item: FilmSearch = dataList.get(position)
            val searchViewHolder = holder as SearchViewHolder
            searchViewHolder.itemview.setOnClickListener() { onClick.onClickItem(film_item) }
            return searchViewHolder.onBindData(film_item)
        }
    }

    override fun getItemCount(): Int {
        if (dataList == null) return 0
        return dataList.size
    }

    fun addFooterLoading() {
        isLoadingAdd = true
        dataList.add(FilmSearch("","","",""))
        notifyDataSetChanged()
    }

    fun removeFooterLoading() {
        isLoadingAdd = false
        var position = dataList.size - 1
        val filmSearch = dataList.get(position)
        if (filmSearch != null) {
            dataList.removeAt(position)
            notifyItemRemoved(position)
        }
    }
}