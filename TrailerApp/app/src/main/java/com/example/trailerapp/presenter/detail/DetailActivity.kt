package com.example.trailerapp.presenter.detail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.adapter.ActorDetailAdapter
import com.example.trailerapp.data.remote.model.Actor
import com.example.trailerapp.data.remote.model.DetailMovie
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.presenter.HomeActivity
import com.example.trailerapp.presenter.trailer.YoutubeAPIActivity
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), DetailContract.View {
    private lateinit var presenter: DetailContract.Presenter
    private var movieId: String = ""
    private lateinit var adapter: ActorDetailAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        getInit()
        getData()
        getAction()
    }

    private fun getInit() {
        presenter = DetailPresenter(this, FirebaseRepository(this))

        var firebaseUser = presenter.getUserCurrent()
        if (firebaseUser == null) {
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        adapter = ActorDetailAdapter()
        detail_activity_rc_actor.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        detail_activity_rc_actor.adapter = adapter
    }

    private fun getData() {
        movieId = intent.getStringExtra("movieid")!!
        presenter.getDetailMovie(movieId)
        presenter.getFavouriteMovie(movieId)
        presenter.getRateMovie(movieId)
    }

    private fun getAction() {
        detail_activity_btn_add_favorite.setOnClickListener{
            presenter.favorite()
        }
        detail_activity_btn_watch.setOnClickListener{
            presenter.watched()
            val intenty = Intent(this, YoutubeAPIActivity::class.java)
            intenty.putExtra("movieid", movieId)
            startActivity(intenty)
        }

        detail_activity_rating.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            presenter.rating(rating)
        }
    }

    override fun updateViewMovie(de: DetailMovie) {
        Glide.with(this@DetailActivity).load(de.image).into(detail_activity_image_film)
        detail_activity_tv_detail_film.text = de.title
        detail_activity_tv_content.text = de.plot

        detail_activity_tv_director.text = de.directors
        detail_activity_tv_genre.text = de.genres
        detail_activity_tv_date.text = de.releaseDate
        detail_activity_tv_country.text = de.countries

        val list = ArrayList<Actor>()
        for (i in 0..(de.starList.size)){
            list.add(de.actorList.get(i))
        }
        adapter.setData(list)

//        presenter.getDetailActor(de.actorList)
    }

//    override fun updateViewActor(list: ArrayList<Actor>) {
//        adapter.setData(list)
//        detail_activity_rc_actor.adapter?.notifyDataSetChanged()
//    }

    override fun updateViewFavorite(check: Boolean) {
        if(check){
            detail_activity_btn_add_favorite.setImageResource(R.drawable.ic_favorite)
        } else {
            detail_activity_btn_add_favorite.setImageResource(R.drawable.ic_favorite_border)
        }
    }

    override fun watchMovie(movieId: String) {
        TODO("Not yet implemented")
    }

    override fun updateViewRating(rate: Float) {
        detail_activity_rating.rating = rate
    }
}