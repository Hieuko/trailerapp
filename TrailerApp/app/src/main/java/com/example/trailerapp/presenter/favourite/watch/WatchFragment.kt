package com.example.trailerapp.presenter.favourite.watch

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trailerapp.R
import com.example.trailerapp.data.adapter.FavouriteAdapter
import com.example.trailerapp.data.adapter.OnClickMovie
import com.example.trailerapp.data.adapter.WatchedAdapter
import com.example.trailerapp.data.remote.model.MovieInformation
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.presenter.HomeActivity
import com.example.trailerapp.presenter.detail.DetailActivity
import com.example.trailerapp.presenter.favourite.favourite.FavouriteContract
import com.example.trailerapp.presenter.favourite.favourite.FavouritePresenter
import kotlinx.android.synthetic.main.fragment_favourite.*
import kotlinx.android.synthetic.main.fragment_watch.*

class WatchFragment : Fragment(), WatchContract.View, OnClickMovie {
    private lateinit var presenter: WatchContract.Presenter
    private lateinit var adapter_watched: WatchedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_watch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = WatchPresenter(this, FirebaseRepository(requireActivity()))
        var firebaseUser = presenter.getUserCurrent()
        if (firebaseUser == null) {
            val intent = Intent(activity, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
        adapter_watched = WatchedAdapter(this)
        watch_fragment_rc.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        watch_fragment_rc.adapter = adapter_watched
        presenter.getWatchMovieList()
    }
    override fun onDestroy() {
        Log.d("KO", "WatchFragment onDestroy() called")
        super.onDestroy()
        presenter.removeReadWatchedList()
    }

    override fun updateViewData(movie: MovieInformation) {
        adapter_watched.addData(movie)
    }

    override fun onClick(movie: MovieInformation) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("movieid", movie.movieid)
        startActivity(intent)
    }

    override fun onClickDelete(movie: MovieInformation) {
        TODO("Not yet implemented")
    }
}