package com.example.trailerapp.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FilmSearchResponse {
    @Expose
    @SerializedName("results")
    val FilmSearchs: ArrayList<FilmSearch>? = null
}