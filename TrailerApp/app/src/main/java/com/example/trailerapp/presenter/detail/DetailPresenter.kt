package com.example.trailerapp.presenter.detail

import android.util.Log
import com.example.trailerapp.data.adapter.OnMovieListener
import com.example.trailerapp.data.remote.model.Star
import com.example.trailerapp.data.remote.model.DetailMovie
import com.example.trailerapp.data.remote.model.MovieInformation
import com.example.trailerapp.data.remote.model.MovieRate
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.data.repository.MovieRepository
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.*
import java.lang.Exception

class DetailPresenter(val view: DetailContract.View, var firebaseRepo: FirebaseRepository): DetailContract.Presenter, OnMovieListener {
    private val jobHome = SupervisorJob()
    private val handler = CoroutineExceptionHandler { coroutineContext, throwable ->

    }
    private val scopeMainHome = CoroutineScope(jobHome + Dispatchers.Main + handler)
    private val scopeIOHome = CoroutineScope(jobHome + Dispatchers.IO + handler)

    private val repository = MovieRepository()

    var movieFavourite: MovieInformation? = null
    var favorite:Boolean? = null
    var movieRate: MovieRate? = null
    var movie: DetailMovie? = null
    var userid: String? = null

    override fun getDetailMovie(movieId: String) {
        scopeIOHome.launch {
            val dataList =  repository.callDetailMovie(movieId)
            movie = dataList
            scopeMainHome.launch() {
                view.updateViewMovie(dataList!!)
            }
        }
    }

//    override fun getDetailActor(starList: ArrayList<Star>){
//        scopeIOHome.launch {
//            val act = repository.callActorDetail(starList)
//            scopeMainHome.launch() {
//                view.updateViewActor(act)
//            }
//        }
//    }

    override fun getUserCurrent(): FirebaseUser? {
        val user = firebaseRepo.checkUser()
        if (user != null) userid = user.uid
        return user
    }

    override fun getFavouriteMovie(movieId: String) {
        firebaseRepo.readMovieFavourite(movieId, this)
    }

    override fun getRateMovie(movieId: String) {
        firebaseRepo.readMovieRate(movieId, this)
    }

    override fun favorite() {
        if (favorite == false){
            favorite = true
            if (movieFavourite == null){
                movieFavourite = MovieInformation(userid!!, movie!!.id, movie!!.title, movie!!.image, System.currentTimeMillis())
            } else {
                movieFavourite!!.time = System.currentTimeMillis()
            }
            firebaseRepo.storeMovieFavourite(movieFavourite!!, movie!!.id)
        } else {
            favorite = false
            firebaseRepo.storeMovieFavourite(null, movie!!.id)
        }
        view.updateViewFavorite(favorite!!)
    }

    override fun watched() {
        val movieWatched = MovieInformation(userid!!, movie!!.id, movie!!.title, movie!!.image, System.currentTimeMillis())
        firebaseRepo.storeMovieWatched(movieWatched)
    }

    override fun rating(rate: Float) {
        if (movieRate == null){
            movieRate = MovieRate(userid, movie!!.id, rate)
            movieRate!!.time = System.currentTimeMillis()
        } else {
            movieRate!!.rate = rate
            movieRate!!.time = System.currentTimeMillis()
        }
        firebaseRepo.storeMovieRate(movieRate!!)
        scopeIOHome.launch {
            try {
                val m = repository.storeRate(movieRate!!)
                if (m == null){
                    Log.d("KO", "Rating Api failure")
                } else {
                    Log.d("KO", "Rating Api success")
                }
            } catch (e: Exception){
                Log.d("KO", "Rating Api failure")
            }

        }
    }

    override fun onMovieWatchListener(movie: MovieInformation?) {

    }

    override fun onMovieWatchChangedListener(movie: MovieInformation?) {
        TODO("Not yet implemented")
    }

    override fun onMovieFavoriteListener(movie: MovieInformation?) {
        if (movie == null){
            favorite = false
            view.updateViewFavorite(favorite!!)
        }
        else {
            this.movieFavourite = movie
            favorite = true
            view.updateViewFavorite(favorite!!)
        }
    }

    override fun onMovieFavoriteRemovedListener(movie: MovieInformation?) {
        TODO("Not yet implemented")
    }

    override fun onMovieRateListener(movie: MovieRate?) {
        if (movie == null){
            view.updateViewRating(0f)
        }
        else {
            this.movieRate = movie
            view.updateViewRating(movie.rate!!)
        }
    }
}