package com.example.trailerapp.presenter.favourite.watch

import com.example.trailerapp.data.adapter.OnMovieListener
import com.example.trailerapp.data.remote.model.MovieInformation
import com.example.trailerapp.data.remote.model.MovieRate
import com.example.trailerapp.data.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class WatchPresenter(val view: WatchContract.View, var repository: FirebaseRepository): WatchContract.Presenter, OnMovieListener {
    var userid: String? = null

    override fun getUserCurrent(): FirebaseUser? {
        val user = repository.checkUser()
        if (user != null) userid = user.uid
        return user
    }

    override fun getWatchMovieList() {
        repository.readMovieWatcheds(this)
    }

    override fun removeReadWatchedList() {
        repository.removeMovieWatcheds()
    }

    override fun onMovieWatchListener(movie: MovieInformation?) {
        view.updateViewData(movie!!)
    }

    override fun onMovieWatchChangedListener(movie: MovieInformation?) {
        view.updateViewData(movie!!)
    }

    override fun onMovieFavoriteListener(movie: MovieInformation?) {
        TODO("Not yet implemented")
    }

    override fun onMovieFavoriteRemovedListener(movie: MovieInformation?) {
        TODO("Not yet implemented")
    }

    override fun onMovieRateListener(movie: MovieRate?) {
        TODO("Not yet implemented")
    }
}