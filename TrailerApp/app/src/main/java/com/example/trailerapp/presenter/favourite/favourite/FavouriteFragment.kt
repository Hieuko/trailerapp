package com.example.trailerapp.presenter.favourite.favourite

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trailerapp.R
import com.example.trailerapp.data.adapter.FavouriteAdapter
import com.example.trailerapp.data.adapter.OnClickMovie
import com.example.trailerapp.data.remote.model.MovieInformation
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.presenter.HomeActivity
import com.example.trailerapp.presenter.detail.DetailActivity
import kotlinx.android.synthetic.main.fragment_favourite.*


class FavouriteFragment : Fragment(), FavouriteContract.View, OnClickMovie{
    private lateinit var presenter:FavouriteContract.Presenter
    private lateinit var adapter_favourite: FavouriteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("KO", "Favourite onCreate() called")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.d(
            "KO",
            "Favourite onCreateView() called"
        )
        return inflater.inflate(R.layout.fragment_favourite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = FavouritePresenter(this, FirebaseRepository(requireActivity()))
        var firebaseUser = presenter.getUserCurrent()
        if (firebaseUser == null) {
            val intent = Intent(activity, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
        adapter_favourite = FavouriteAdapter(this)
        favourite_fragment_rc.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        favourite_fragment_rc.adapter = adapter_favourite
        presenter.getFavouriteMovieList()
    }


    override fun onDestroyView() {
        Log.d("KO", "Favourite onDestroyView() called")
        super.onDestroyView()
    }
    override fun onDestroy() {
        Log.d("KO", "Favourite onDestroy() called")
        super.onDestroy()
        presenter.removeReadFavouriteList()
    }

    override fun onPause() {
        Log.d("KO", "Favourite onPause() called")
        super.onPause()
    }

    override fun onResume() {
        Log.d("KO", "Favourite onResume() called")
        super.onResume()
    }

    override fun onStart() {
        Log.d("KO", "Favourite onStart() called")
        super.onStart()
    }

    override fun onStop() {
        Log.d("KO", "Favourite onStop() called")
        super.onStop()
    }

    override fun updateViewData(movie: MovieInformation) {
        adapter_favourite.addData(movie)
    }

    override fun removeViewData(movie: MovieInformation) {
        adapter_favourite.removeData(movie)
    }

    override fun onClick(movie: MovieInformation) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("movieid", movie.movieid)
        startActivity(intent)
    }

    override fun onClickDelete(movie: MovieInformation) {
        presenter.deleteFavorite(movie)
    }
}