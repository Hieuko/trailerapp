package com.example.trailerapp.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TrailerVideo (
    @Expose
    @SerializedName("imDbId")
    val movieid: String,
    @Expose
    @SerializedName("title")
    val title: String,
    @Expose
    @SerializedName("videoId")
    val videoId: String,
    @Expose
    @SerializedName("videoUrl")
    val videoUrl: String,
): Serializable