package com.example.trailerapp.presenter.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.trailerapp.R
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.presenter.BaseFragment
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : BaseFragment(), RegisterContract.View {
    private lateinit var presenter:RegisterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = RegisterPresenter(this, FirebaseRepository(requireActivity()))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideKeyboard(requireView())
        register_fragment_btn_register.setOnClickListener {
            val email = register_fragment_edt_email.text.toString()
            val password = register_fragment_edt_password.text.toString()
            val confirmPassword = register_fragment_edt_confirmpass.text.toString()
            val username = register_fragment_edt_username.text.toString()

            presenter.register(email, password, confirmPassword, username)
        }

        register_fragment_btn_cancel.setOnClickListener {
            it.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

    override fun updateUI(message: String) {
        when(message){
            "e_null" -> {
                register_fragment_edt_email.error = "Require Email"
            }
            "e_invalid" -> {
                register_fragment_edt_email.error = "Email format abc@gmail"
                register_fragment_edt_email.setText("")
            }
            "p_null" -> {
                register_fragment_edt_password.error = "Require Password"
            }
            "p_invalid" -> {
                register_fragment_edt_password.error = "6 char password required"
                register_fragment_edt_password.setText("")
            }
            "pc_null"->{
                register_fragment_edt_confirmpass.error = "Require Confirm Password"
            }
            "pass_not_confirm"->{
                register_fragment_edt_password.setText("")
                register_fragment_edt_confirmpass.setText("")
                Toast.makeText(context, "Khong khop password", Toast.LENGTH_LONG)
                    .show()
            }
            "username_null"-> {
                register_fragment_edt_username.error = "Require Username"
            }
            "success" -> {
                this.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
            }
            "failure" -> Toast.makeText(context, "Tài khoản đã tồn tại", Toast.LENGTH_LONG)
                .show()
        }
    }

}