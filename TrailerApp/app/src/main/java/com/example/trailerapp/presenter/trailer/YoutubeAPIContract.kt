package com.example.trailerapp.presenter.trailer

interface YoutubeAPIContract {
    interface View{
        fun updateViewData(videoId: String)
    }

    interface Presenter{
        fun getVideoTrailer(movieId:String)
    }
}