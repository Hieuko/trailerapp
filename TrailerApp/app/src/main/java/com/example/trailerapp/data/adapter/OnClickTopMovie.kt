package com.example.trailerapp.data.adapter

import com.example.trailerapp.data.remote.model.DetailMovie
import com.example.trailerapp.data.remote.model.TopMovie

interface OnClickTopMovie {
    fun clickOnItem(m: TopMovie)
    fun clickOnItem(m: DetailMovie)
}