package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.DetailMovie
import kotlinx.android.synthetic.main.layout_top_movie.view.*

class RecommenAdpater(var onClick: OnClickTopMovie): RecyclerView.Adapter<RecommenAdpater.ViewHolder>() {
    private var dataList = ArrayList<DetailMovie>()

    fun setData(data: ArrayList<DetailMovie>) {
        dataList = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_top_movie, parent, false)
        return RecommenAdpater.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val m = dataList.get(position)
        holder.onBindData(m)
        holder.itemView.setOnClickListener(
            View.OnClickListener {
                onClick.clickOnItem(m)
            }
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBindData(m: DetailMovie) {
            itemView.txt_film.text = m.title
            Glide.with(itemView.context).load(m.image).placeholder(R.drawable.load_image)
                .into(itemView.image_popular)
        }
    }
}