package com.example.trailerapp.presenter.profile

import android.net.Uri
import android.util.Log
import com.example.trailerapp.data.remote.model.User
import com.example.trailerapp.data.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class ProfilePresenter(val view: ProfileContract.View,var repository: FirebaseRepository):ProfileContract.Presenter, ProfileContract.OnProfileListener {
    var user: User? = null

    override fun readUser() {
        Log.d("KO", "ProfilePresenter readUser() called")
        repository.readUserDetail(this)
    }

    override fun logout() {
        repository.logout()
    }

    override fun removeReadUser() {
        repository.removeUserDetail()
    }

    override fun updateUser(user: User) {
        repository.storeUser(user)
    }

    override fun getUserCurrent(): FirebaseUser? {
        return repository.checkUser()
    }

    override fun uploadImage(imageURI: Uri?, ex: String?) {
        repository.storageImage(imageURI, ex)
    }

    override fun updateUserListen(user: User) {
        this.user = user
        view.updateViewData(user)
    }
}