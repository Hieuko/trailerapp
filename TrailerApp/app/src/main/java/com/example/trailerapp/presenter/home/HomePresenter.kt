package com.example.trailerapp.presenter.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trailerapp.data.remote.model.TopMovie
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.data.repository.MovieRepository
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.*

class HomePresenter(private val view: HomeContract.View, var firebaseRepo: FirebaseRepository) : HomeContract.Presenter {
    private val jobHome = SupervisorJob()
    private val handler = CoroutineExceptionHandler { coroutineContext, throwable ->

    }
    private val scopeMainHome = CoroutineScope(jobHome + Dispatchers.Main + handler)
    private val scopeIOHome = CoroutineScope(jobHome + Dispatchers.IO + handler)

    private val repository = MovieRepository()

    private var popularMovies = ArrayList<TopMovie>()
    private var newMovies = ArrayList<TopMovie>()

    var userid: String? = null

    override fun getUserCurrent(): FirebaseUser? {
        val user = firebaseRepo.checkUser()
        if (user != null) userid = user.uid
        return user
    }

    override fun getMostPopularMovieApi() {
        scopeIOHome.launch {
            val dataList = repository.callMostPopularMovie()
            popularMovies = dataList!!
            scopeMainHome.launch() {
                view.updateViewDataMostPopularMovie(dataList!!)
            }
        }
    }

    override fun getMostPopularMovie(){
        if (popularMovies.isNullOrEmpty()){
            getMostPopularMovieApi()
        } else {
            scopeMainHome.launch() {
                view.updateViewDataMostPopularMovie(popularMovies)
            }
        }
    }

    override fun getNewFilmApi() {
        scopeIOHome.launch {
            val dataList = repository.callNewFilm()
            newMovies = dataList!!
            scopeMainHome.launch() {
                view.updateViewDataNewFilm(newMovies)
            }
        }
    }

    override fun getNewFilm() {
        if (newMovies.isNullOrEmpty()){
            getNewFilmApi()
        } else {
            scopeMainHome.launch() {
                view.updateViewDataNewFilm(newMovies)
            }
        }
    }

    override fun getRecommendApi() {
        scopeIOHome.launch {
            val dataList = repository.callRecommend(userid!!)
            if (!dataList.list.isNullOrEmpty()){
                val data = repository.callMovieRecommend(dataList.list!!)
                scopeMainHome.launch() {
                    view.updateViewDataRecommend(data!!)
                }
            } else{
                scopeMainHome.launch() {
                    view.updateViewDataRecommend(null)
                }
            }
        }
    }


    override fun cleanup() {
        jobHome.cancel()
    }
}