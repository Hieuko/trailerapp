package com.example.trailerapp.presenter.register

interface RegisterContract {
    interface View{
        fun updateUI(message: String)
    }
    interface Presenter{
        fun register(email: String, password: String, confirmPassword: String, username: String)
    }
    interface OnRegisterListener{
        fun onSuccess(message: String?)
        fun onFailure(message: String?)
    }
}