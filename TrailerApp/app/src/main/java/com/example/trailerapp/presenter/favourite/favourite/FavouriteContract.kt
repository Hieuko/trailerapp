package com.example.trailerapp.presenter.favourite.favourite

import com.example.trailerapp.data.remote.model.MovieInformation
import com.google.firebase.auth.FirebaseUser

interface FavouriteContract {
    interface View{
        fun updateViewData(movie: MovieInformation)
        fun removeViewData(movie: MovieInformation)
    }

    interface Presenter{
        fun getUserCurrent(): FirebaseUser?
        fun getFavouriteMovieList()
        fun removeReadFavouriteList()

        fun deleteFavorite(movie: MovieInformation)
    }
}