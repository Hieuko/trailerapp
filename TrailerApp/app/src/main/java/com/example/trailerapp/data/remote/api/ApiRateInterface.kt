package com.example.trailerapp.data.remote.api

import com.example.trailerapp.data.remote.model.ListRecommend
import com.example.trailerapp.data.remote.model.MovieRate
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiRateInterface {
    @GET("user/{user_id}")
    suspend fun getRecommenLists(@Path("user_id") user_id: String): ListRecommend

    @POST("rating/")
    suspend fun rating(@Body movieRate: MovieRate): MovieRate
}