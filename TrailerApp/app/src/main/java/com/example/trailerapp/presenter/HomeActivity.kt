package com.example.trailerapp.presenter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.trailerapp.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}