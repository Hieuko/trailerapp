package com.example.trailerapp.data.adapter

import com.example.trailerapp.data.remote.model.MovieInformation

interface OnClickMovie {
    fun onClick(movie: MovieInformation)
    fun onClickDelete(movie: MovieInformation)
}