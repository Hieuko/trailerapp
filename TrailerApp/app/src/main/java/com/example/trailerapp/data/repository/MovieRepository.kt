package com.example.trailerapp.data.repository

import android.util.Log
import com.example.trailerapp.data.remote.api.ApiClient
import com.example.trailerapp.data.remote.api.ApiRateClient
import com.example.trailerapp.data.remote.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class MovieRepository() {

    suspend fun callMostPopularMovie(): ArrayList<TopMovie>? =
        withContext(Dispatchers.IO) {
            val populars = ApiClient.getClient.getMostPopularMovies()
            populars.topMovies
        }

    suspend fun callNewFilm(): ArrayList<TopMovie>? =
        withContext(Dispatchers.IO) {
            val newfilms = ApiClient.getClient.getNewFilm()
            newfilms.topMovies
        }

    suspend fun callDetailMovie(movieid: String): DetailMovie =
        withContext(Dispatchers.IO) {
            val film = ApiClient.getClient.getDetailMovie(movieId = movieid)
            film
        }

    suspend fun callMovieRecommend(list: ArrayList<String>): ArrayList<DetailMovie>{
        val dataList = ArrayList<DetailMovie>()
        for (i in list) {
            withContext(Dispatchers.IO) {
                val film = ApiClient.getClient.getDetailMovie(movieId = i)
                dataList.add(film)
            }
        }
        Log.d("KO", "callMovieRecommend() called with: list = ${dataList.toString()}")
        return dataList
    }

//    suspend fun callActorDetail(list: ArrayList<Star>): ArrayList<Actor> {
//        val dataList = ArrayList<Actor>()
//        for (i in list) {
//            withContext(Dispatchers.IO) {
//                val actorDetail = ApiClient.getClient.getActorDetail(actor_id = i.id)
//                dataList.add(actorDetail)
//            }
//        }
//        return dataList
//    }

    suspend fun callVideoTrailer(movieid: String): String =
        withContext(Dispatchers.IO) {
            val video = ApiClient.getClient.getVideoTrailer(movieId = movieid)
            video.videoId
        }

    suspend fun callFilmSearch(search_string: String): ArrayList<FilmSearch>? =
        withContext(Dispatchers.IO) {
            val searchs = ApiClient.getClient.getSearchMovie(search_string = search_string)
            searchs.FilmSearchs
        }

    suspend fun callNoFilmSearch(): ArrayList<TopMovie>? =
        withContext(Dispatchers.IO) {
            val tops = ApiClient.getClient.getNoSearchMovie()
            tops.topMovies
        }

    suspend fun callRecommend(user_id: String): ListRecommend =
        withContext(Dispatchers.IO) {
            val tops = ApiRateClient.getClient.getRecommenLists(user_id)
            Log.d("KO", "callRecommend() called ${tops.list.toString()}")
            tops
        }

    suspend fun storeRate(movie: MovieRate) =
        withContext(Dispatchers.IO) {
            val m = ApiRateClient.getClient.rating(movie)
            m
        }
}