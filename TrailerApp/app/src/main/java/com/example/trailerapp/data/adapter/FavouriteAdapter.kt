package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.MovieInformation
import kotlinx.android.synthetic.main.layout_favourite.view.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class FavouriteAdapter(val onclick:OnClickMovie): RecyclerView.Adapter<FavouriteAdapter.ViewHolder>() {
    private var listdata = ArrayList<MovieInformation>()
    fun setDataList(list :ArrayList<MovieInformation>){
        this.listdata=list
    }
    fun addData(movie: MovieInformation){
        val index = checkMovie(movie)
        if (checkMovie(movie) == -1){
            listdata.add(movie)
        } else{
            listdata.removeAt(index)
            listdata.add(movie)
        }
        Collections.sort(listdata, object :Comparator<MovieInformation>{
            override fun compare(p0: MovieInformation?, p1: MovieInformation?): Int {
                if (p0!!.time!! > p1!!.time!!) return -1
                else if (p0!!.time!! == p1!!.time!!) return 0
                else return 1
            }
        })
        notifyDataSetChanged()
    }
    fun removeData(movie: MovieInformation){
        if (listdata.contains(movie)){
            listdata.remove(movie)
            notifyDataSetChanged()
        }
    }

    private fun checkMovie(movie: MovieInformation): Int{
        if (!listdata.isEmpty()){
            for (i in 0..(listdata.size-1)){
                if (listdata.get(i).movieid == movie.movieid){
                    return i
                }
            }
        }
        return -1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_favourite,parent,false))
    }

    override fun onBindViewHolder(holder: FavouriteAdapter.ViewHolder, position: Int) {
        val m = listdata[position]
        holder.onBind(m)
        holder.itemView.favourite_layout_favourite.setOnClickListener {
            onclick.onClickDelete(m)
        }
        holder.itemView.setOnClickListener {
            onclick.onClick(m)
        }
    }

    override fun getItemCount(): Int {
        return listdata.size
    }

    inner class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        fun onBind(movie: MovieInformation){
            itemView.favourite_layout_title.text = movie.title
            Glide.with(itemView.context).load(movie.img).into(itemView.favourite_layout_img)
        }
    }

}