package com.example.trailerapp.presenter.favourite

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.example.trailerapp.R
import com.example.trailerapp.data.adapter.TabPagerApdapter
import com.example.trailerapp.data.adapter.ViewPagerAdapter
import com.example.trailerapp.presenter.favourite.favourite.FavouriteFragment
import com.example.trailerapp.presenter.favourite.watch.WatchFragment
import com.example.trailerapp.presenter.home.HomeFragment
import com.example.trailerapp.presenter.profile.ProfileFragment
import com.example.trailerapp.presenter.search.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_information.*

class InformationFragment : Fragment() {
    private var listInformationFragment = ArrayList<Fragment>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
//        initAction()
    }

    private fun initData() {
        listInformationFragment =
            arrayListOf(FavouriteFragment(), WatchFragment())
        val tabPagerAdapter = TabPagerApdapter(listInformationFragment, childFragmentManager)
        information_fragment_viewpager.adapter = tabPagerAdapter
        information_fragment_tablayout.setupWithViewPager(information_fragment_viewpager)
        information_fragment_tablayout.getTabAt(0)!!.setIcon(R.drawable.ic_favorite)
        information_fragment_tablayout.getTabAt(1)!!.setIcon(R.drawable.movie)
    }

//    private fun initAction() {
//        main_activity_bottom_navigation.setOnNavigationItemSelectedListener {
//            when (it.itemId) {
//                R.id.ic_movie -> {
//                    main_activity_viewPager.currentItem = 0
//                }
//                R.id.ic_search -> {
//                    main_activity_viewPager.currentItem = 1
//                }
//                R.id.ic_infor -> {
//                    main_activity_viewPager.currentItem = 2
//                }
//                R.id.ic_profile -> {
//                    main_activity_viewPager.currentItem = 3
//                }
//            }
//            true
//        }
//
//        main_activity_viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
//            override fun onPageScrollStateChanged(state: Int) {
//            }
//
//            override fun onPageScrolled(
//                position: Int,
//                positionOffset: Float,
//                positionOffsetPixels: Int
//            ) {
//            }
//
//            override fun onPageSelected(position: Int) {
//                when (position) {
//                    0 -> {
//                        main_activity_bottom_navigation.menu.findItem(R.id.ic_movie)
//                            .setChecked(true)
//                    }
//                    1 -> {
//                        main_activity_bottom_navigation.menu.findItem(R.id.ic_search)
//                            .setChecked(true)
//                    }
//                    2 -> {
//                        main_activity_bottom_navigation.menu.findItem(R.id.ic_infor)
//                            .setChecked(true)
//                    }
//                    3 -> {
//                        main_activity_bottom_navigation.menu.findItem(R.id.ic_profile)
//                            .setChecked(true)
//                    }
//                }
//            }
//        })
//
//        main_activity_viewPager.setCurrentItem(3)
//    }

    override fun onDestroyView() {
        Log.d("KO", "Information onDestroyView() called")
        super.onDestroyView()
    }
    override fun onDestroy() {
        Log.d("KO", "Information onDestroy() called")
        super.onDestroy()
    }

    override fun onPause() {
        Log.d("KO", "Information onPause() called")
        super.onPause()
    }

    override fun onResume() {
        Log.d("KO", "Information onResume() called")
        super.onResume()
    }

    override fun onStart() {
        Log.d("KO", "Information onStart() called")
        super.onStart()
    }

    override fun onStop() {
        Log.d("KO", "Information onStop() called")
        super.onStop()
    }
}