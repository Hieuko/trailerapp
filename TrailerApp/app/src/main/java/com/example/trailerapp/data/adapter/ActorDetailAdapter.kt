package com.example.trailerapp.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.Actor
import kotlinx.android.synthetic.main.layout_actor_item.view.*

class ActorDetailAdapter: RecyclerView.Adapter<ActorDetailAdapter.ActorViewHolder>() {
    private var dataList = ArrayList<Actor>()

    fun setData(list: ArrayList<Actor>){
        this.dataList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActorDetailAdapter.ActorViewHolder {
        return ActorViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_actor_item, parent, false))
    }

    override fun onBindViewHolder(holder: ActorDetailAdapter.ActorViewHolder, position: Int) {
        val dataModel = dataList.get(position)
        holder.bindData(dataModel)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ActorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(a: Actor) {
            itemView.actor_item_layout_txt_name.text = a.name
            Glide.with(itemView.context).load(a.image).into(itemView.actor_item_layout_img)
        }
    }
}