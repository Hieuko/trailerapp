package com.example.trailerapp.data.remote.api

import com.example.trailerapp.data.remote.model.*
import com.example.trailerapp.data.util.Contants
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    @GET("MostPopularMovies/{api_key}")
    suspend fun getMostPopularMovies(@Path("api_key") api_key: String = Contants.API_KEY): TopMoviesResponse

    @GET("ComingSoon/{api_key}")
    suspend fun getNewFilm(@Path("api_key") api_key: String = Contants.API_KEY): TopMoviesResponse

    @GET("Top250Movies/{api_key}")
    suspend fun getNoSearchMovie(@Path("api_key") api_key: String = Contants.API_KEY): TopMoviesResponse

    @GET("Title/{api_key}/{movieId}")
    suspend fun getDetailMovie(
        @Path("api_key") api_key: String = Contants.API_KEY,
        @Path("movieId") movieId: String
    ): DetailMovie

    @GET("YouTubeTrailer/{api_key}/{movieId}")
    suspend fun getVideoTrailer(
        @Path("api_key") api_key: String = Contants.API_KEY,
        @Path("movieId") movieId: String
    ): TrailerVideo

//    @GET("Name/{api_key}/{actorId}")
//    suspend fun getActorDetail(
//        @Path("api_key") api_key: String = Contants.API_KEY,
//        @Path("actorId") actor_id: String
//    ): Actor

    @GET("SearchMovie/{api_key}/{search_string}")
    suspend fun getSearchMovie(
        @Path("api_key") api_key: String = Contants.API_KEY,
        @Path("search_string") search_string: String
    ): FilmSearchResponse
}