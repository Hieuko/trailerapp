package com.example.trailerapp.presenter.favourite.favourite

import com.example.trailerapp.data.adapter.OnMovieListener
import com.example.trailerapp.data.remote.model.MovieInformation
import com.example.trailerapp.data.remote.model.MovieRate
import com.example.trailerapp.data.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class FavouritePresenter(val view: FavouriteContract.View, var repository: FirebaseRepository): FavouriteContract.Presenter, OnMovieListener {
    var userid: String? = null

    override fun getUserCurrent(): FirebaseUser? {
        val user = repository.checkUser()
        if (user != null) userid = user.uid
        return user
    }

    override fun getFavouriteMovieList() {
        repository.readMovieFavourites(this)
    }

    override fun removeReadFavouriteList() {
        repository.removeMovieFavourites()
    }

    override fun deleteFavorite(movie: MovieInformation) {
        repository.storeMovieFavourite(null, movie.movieid!!)
    }

    override fun onMovieWatchListener(movie: MovieInformation?) {
        TODO("Not yet implemented")
    }

    override fun onMovieWatchChangedListener(movie: MovieInformation?) {
        TODO("Not yet implemented")
    }

    override fun onMovieFavoriteListener(movie: MovieInformation?) {
        view.updateViewData(movie!!)
    }

    override fun onMovieFavoriteRemovedListener(movie: MovieInformation?) {
        view.removeViewData(movie!!)
    }

    override fun onMovieRateListener(movie: MovieRate?) {
        TODO("Not yet implemented")
    }
}