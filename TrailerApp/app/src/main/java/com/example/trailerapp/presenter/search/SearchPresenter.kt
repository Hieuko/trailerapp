package com.example.trailerapp.presenter.search

import android.util.Log
import com.example.trailerapp.data.remote.model.TopMovie
import com.example.trailerapp.data.repository.MovieRepository
import kotlinx.coroutines.*
import kotlin.math.log

class SearchPresenter(private val view: SearchContract.View): SearchContract.Presenter {
    private val TAG = "KO Search"
    private val jobSeach = SupervisorJob()
    private var job: Job? = null
    private val handler = CoroutineExceptionHandler { coroutineContext, throwable ->

    }
    private val scopeMainSeach = CoroutineScope(jobSeach + Dispatchers.Main + handler)
    private val scopeIOSeach = CoroutineScope(jobSeach + Dispatchers.IO + handler)

    private val repository = MovieRepository()

    private var noSearchs = ArrayList<TopMovie>()

    override fun getSearchMovie(search: String) {
        Log.d(TAG, "getSearchMovie() called with: search = $search")
        scopeIOSeach.launch {
            val dataList = repository.callFilmSearch(search)
            scopeMainSeach.launch() {
                view.updateViewDataSearch(dataList!!)
            }
        }
    }

    override fun getNoSearchMovieApi() {
        scopeIOSeach.launch {
            val dataList = repository.callNoFilmSearch()
            noSearchs = dataList!!
            scopeMainSeach.launch() {
                view.updateViewDataNoSearch(dataList!!)
            }
        }
    }

    override fun getNoSearchMovie() {
        Log.d(TAG, "getNoSearchMovie() called")
        if (noSearchs.isNullOrEmpty()){
            getNoSearchMovieApi()
        } else {
            scopeMainSeach.launch() {
                view.updateViewDataNoSearch(noSearchs)
            }
        }
    }

    override fun cancelJob() {
        job?.cancel()
    }


}