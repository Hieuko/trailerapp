package com.example.trailerapp.presenter.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.User
import kotlinx.android.synthetic.main.dialog_update.*

class DialogUpdate(val user: User) : DialogFragment() {
    private var dialogListener: UpdateDialogListener? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_update,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false
        update_dialog_email.setText(user.email)
        update_dialog_email.isEnabled = false
        update_dialog_username.setText(user.username)
        update_dialog_country.setText(user.country)
        update_dialog_fullname.setText(user.fullname)
        getAction()
    }

    private fun getAction() {
        update_dialog_btn_update.setOnClickListener{
            val email = update_dialog_email.text.toString()
            val username = update_dialog_username.text.toString()
            val country = update_dialog_country.text.toString()
            val fullname = update_dialog_fullname.text.toString()
            if (!username.isNullOrEmpty()) {
                user.username = username
                user.country = country
                user.fullname = fullname
                dialogListener?.onConfirmButtonClicked(user)
                dismiss()
            } else {
                update_dialog_username.error = "Username Require"
            }
        }
        update_dialog_btn_cancel.setOnClickListener {
            dismiss()
        }
    }

    fun setDialogListener(listener: UpdateDialogListener) {
        this.dialogListener = listener
    }
}
interface UpdateDialogListener{
    fun onConfirmButtonClicked(user: User)
}