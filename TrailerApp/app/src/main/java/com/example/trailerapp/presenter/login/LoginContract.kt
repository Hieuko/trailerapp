package com.example.trailerapp.presenter.login

import com.google.firebase.auth.FirebaseUser

interface LoginContract {
    interface View{
        fun updateUI(message: String)
    }
    interface Presenter{
        fun checkLogin(username: String, password: String)
        fun getUserCurrent(): FirebaseUser?
    }
    interface OnLoginListener{
        fun onSuccess(message: String?)
        fun onFailure(message: String?)
    }
}