package com.example.trailerapp.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TopMoviesResponse {
    @Expose
    @SerializedName("items")
    val topMovies: ArrayList<TopMovie>? = null
}