package com.example.trailerapp.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ListRecommend(
    @Expose
    @SerializedName("list")
    val list: ArrayList<String>?
    )