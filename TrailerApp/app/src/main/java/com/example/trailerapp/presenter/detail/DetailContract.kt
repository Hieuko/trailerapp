package com.example.trailerapp.presenter.detail

import com.example.trailerapp.data.remote.model.*
import com.google.firebase.auth.FirebaseUser

interface DetailContract {
    interface View{
        fun updateViewMovie(de: DetailMovie)
//        fun updateViewActor(list: ArrayList<Actor>)
        fun updateViewFavorite(check: Boolean)
        fun watchMovie(movieId: String)
        fun updateViewRating(rate: Float)
    }

    interface Presenter{
        fun getDetailMovie(movieId: String)
//        fun getDetailActor(starList: ArrayList<Star>)
        fun getUserCurrent(): FirebaseUser?
        fun getFavouriteMovie(movieId: String)
        fun getRateMovie(movieId: String)

        fun favorite()
        fun watched()
        fun rating(rate: Float)
    }
}