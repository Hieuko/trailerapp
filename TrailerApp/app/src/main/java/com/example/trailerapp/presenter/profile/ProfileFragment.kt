package com.example.trailerapp.presenter.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.trailerapp.R
import com.example.trailerapp.data.remote.model.User
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.presenter.HomeActivity
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment(), ProfileContract.View {
    private val TAG = "KO"
    private val REQUEST_READ_STORAGE = 112
    private val IMAGE_REQUEST = 1

    private lateinit var presenter: ProfilePresenter

    private val updateListener = object : UpdateDialogListener {
        override fun onConfirmButtonClicked(user: User) {
            presenter.updateUser(user)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("KO", "Profile onCreate() called")
        presenter = ProfilePresenter(this, FirebaseRepository(requireActivity()))
        var firebaseUser = presenter.getUserCurrent()
        if (firebaseUser == null) {
            val intent = Intent(activity, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.d("KO", "Profile onCreateView() called")
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(
            TAG,
            "Profile onViewCreated() called"
        )
        profile_fragment_image.setOnClickListener {
            selectImage()
        }
        btn_edit_profile.setOnClickListener {
            val updateFrag = DialogUpdate(presenter.user!!)
            updateFrag.setDialogListener(updateListener)
            // show thì show 1 cái dialog vừa màn hình
            updateFrag.show(childFragmentManager, DialogUpdate::class.java.name)
        }
        profile_fragment_btn_logout.setOnClickListener {
            presenter.logout()
            val intent = Intent(requireContext(), HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        presenter.readUser()
    }

    private fun selectImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val hasPermission = ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
            if (!hasPermission) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    REQUEST_READ_STORAGE
                )
                Log.d(TAG, "Request permisson")
            } else {
                readImage()
                Log.d(TAG, "Permission successed")
            }
        }
    }

    private fun readImage() {
        val intentImage = Intent()
        intentImage.type = "image/*"
        intentImage.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intentImage, "Select Picture"), IMAGE_REQUEST)
    }

    private fun getFileExtention(uri: Uri): String? {
        val contentResolver = requireContext().contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(
            TAG,
            "onActivityResult() called with: requestCode = $requestCode, resultCode = $resultCode, data = $data"
        )
        if (requestCode == IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val imageURI = data.data
            val ex = getFileExtention(imageURI!!)
            Log.d(
                TAG,
                "onActivityResult() called with: imageURI  = ${imageURI.toString()}"
            )
            upLoadMyImage(imageURI, ex)
        }
    }

    private fun upLoadMyImage(imageURI: Uri?, ex: String?) {
        Log.d(TAG, "upLoadMyImage() called with: imageURI = $imageURI, ex = $ex")
        presenter.uploadImage(imageURI, ex)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        Log.d(
            TAG,
            "onRequestPermissionsResult() called with: requestCode = $requestCode, permissions = ${permissions[0]}, grantResults = ${grantResults[0]}"
        )
        if (requestCode == REQUEST_READ_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                readImage()
                Log.d(TAG, "Permission success")
            } else {
                Toast.makeText(requireContext(), "You don't assign permission.", Toast.LENGTH_SHORT)
                    .show()
                Log.d(
                    TAG,
                    "onRequestPermissionsResult() failure"
                )
            }
        }
    }

    override fun updateViewData(user: User) {
        profile_fragment_username.setText(user.username)
        if (user.imgURL == "default") {
            profile_fragment_image.setImageResource(R.drawable.ic_profile)
        } else {
            Glide.with(requireContext()).load(user.imgURL).into(profile_fragment_image)
        }
    }

//    private fun takePictureIntent() {
//        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { pictureIntent ->
//            pictureIntent.resolveActivity(activity?.packageManager!!)?.also {
//                startActivityForResult(pictureIntent, REQUEST_IMAGE_CAPTURE)
//            }
//        }
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            val imageBitmap = data?.extras?.get("data") as Bitmap
//            uploadImageAndSaveUri(imageBitmap)
//        }
//    }
//
//    private fun uploadImageAndSaveUri(bitmap: Bitmap) {
//        val baos = ByteArrayOutputStream()
//        val storageRef = FirebaseStorage.getInstance()
//            .reference
//            .child("pics/${FirebaseAuth.getInstance().currentUser?.uid}")
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
//        val image = baos.toByteArray()
//
//        val upload = storageRef.putBytes(image)
//
//        progressbar_pic.visibility = View.VISIBLE
//        upload.addOnCompleteListener { uploadTask ->
//            progressbar_pic.visibility = View.INVISIBLE
//
//            if (uploadTask.isSuccessful) {
//                storageRef.downloadUrl.addOnCompleteListener { urlTask ->
//                    urlTask.result?.let {
//                        imageUri = it
//                        activity?.toast(imageUri.toString())
//                        image_view.setImageBitmap(bitmap)
//                    }
//                }
//            } else {
//                uploadTask.exception?.let {
//                    activity?.toast(it.message!!)
//                }
//            }
//        }
//
//    }

    //    override fun onDestroyView() {
//        Log.d("KO", "Profile onDestroyView() called")
//        super.onDestroyView()
//    }
    override fun onDestroy() {
        Log.d("KO", "Profile onDestroy() called")
        super.onDestroy()
        presenter.removeReadUser()
    }
//
//    override fun onPause() {
//        Log.d("KO", "Profile onPause() called")
//        super.onPause()
//    }
//
//    override fun onResume() {
//        Log.d("KO", "Profile onResume() called")
//        super.onResume()
//    }
//
    override fun onStart() {
        Log.d("KO", "Profile onStart() called")
        super.onStart()
    }
//
//    override fun onStop() {
//        Log.d("KO", "Profile onStop() called")
//        super.onStop()
//    }
}