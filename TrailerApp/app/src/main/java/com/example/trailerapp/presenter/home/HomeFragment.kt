package com.example.trailerapp.presenter.home

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.trailerapp.R
import com.example.trailerapp.data.adapter.OnClickTopMovie
import com.example.trailerapp.data.adapter.RecommenAdpater
import com.example.trailerapp.data.adapter.SliderAdapter
import com.example.trailerapp.data.adapter.TopMovieAdapter
import com.example.trailerapp.data.remote.model.DetailMovie
import com.example.trailerapp.data.remote.model.TopMovie
import com.example.trailerapp.data.repository.FirebaseRepository
import com.example.trailerapp.presenter.HomeActivity
import com.example.trailerapp.presenter.detail.DetailActivity
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.*


class HomeFragment : Fragment(), HomeContract.View, OnClickTopMovie {
    private lateinit var presenter: HomeContract.Presenter
    private lateinit var adapter_popular: TopMovieAdapter
    private lateinit var adapter_newfilm: TopMovieAdapter
    private lateinit var adapter_recommend: RecommenAdpater
    private lateinit var adapter_slide: SliderAdapter

    private var isLoadingPopular = false
    private var isLastPagePopular = false
    private var totalPagePopular = 0
    private var currentPagePopular = 1
    private var popularList = ArrayList<TopMovie>()

    private var isLoadingNew = false
    private var isLastPageNew = false
    private var totalPageNew = 0
    private var currentPageNew = 1
    private var newList = ArrayList<TopMovie>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = HomePresenter(this, FirebaseRepository(requireActivity()))
        adapter_popular = TopMovieAdapter(this)
        adapter_newfilm = TopMovieAdapter(this)
        adapter_recommend = RecommenAdpater(this)
        adapter_slide = SliderAdapter(this)
        Log.d("KO", "Home onCreate() called")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.d("KO", "Home onCreateView() called")
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var firebaseUser = presenter.getUserCurrent()
        if (firebaseUser == null) {
            val intent = Intent(requireContext(), HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        val itemDecoration =
            DividerItemDecoration(requireContext(), DividerItemDecoration.HORIZONTAL)

        home_fragment_rc_popular.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        home_fragment_rc_popular.adapter = adapter_popular
        home_fragment_rc_popular.addItemDecoration(itemDecoration)
        home_fragment_rc_popular.isFocusable = false
        home_fragment_rc_popular.isNestedScrollingEnabled = false
        presenter.getMostPopularMovie()

        home_fragment_slide.setSliderAdapter(adapter_slide)

        home_fragment_rc_new.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        home_fragment_rc_new.adapter = adapter_newfilm
        home_fragment_rc_new.addItemDecoration(itemDecoration)
        home_fragment_rc_new.isFocusable = false
        home_fragment_rc_new.isNestedScrollingEnabled = false
        presenter.getNewFilm()

        home_fragment_rc_recommend.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        home_fragment_rc_recommend.adapter = adapter_recommend
        home_fragment_rc_recommend.addItemDecoration(itemDecoration)
        home_fragment_rc_recommend.isFocusable = false
        home_fragment_rc_recommend.isNestedScrollingEnabled = false
        presenter.getRecommendApi()
    }

    override fun updateViewDataMostPopularMovie(dataList: ArrayList<TopMovie>) {
        if (!dataList.isNullOrEmpty()) {
            val size = dataList.size
            totalPagePopular = if (size % 10 == 0) size / 10 else size / 10 + 1
            val linearLayoutManager = home_fragment_rc_popular.layoutManager as LinearLayoutManager
            setFistDataPopular(dataList)
            home_fragment_rc_popular.addOnScrollListener(object :
                PaginationScrollListener(linearLayoutManager!!) {
                override fun loadMoreItems() {
                    isLoadingPopular = true
                    currentPagePopular += 1
                    loadNextPagePopular(dataList)
                }

                override fun isLoading(): Boolean {
                    return isLoadingPopular
                }

                override fun isLastPage(): Boolean {
                    return isLastPagePopular
                }
            })
        }
    }

    override fun updateViewDataNewFilm(dataList: ArrayList<TopMovie>) {
        if (dataList.isEmpty()) {
            home_fragment_layout_newmovie.visibility = View.GONE
        } else {
            setSlide(dataList)

            val size = dataList.size
            totalPageNew = if (size % 10 == 0) size / 10 else size / 10 + 1
            val linearLayoutManager = home_fragment_rc_new.layoutManager as LinearLayoutManager
            setFistDataNew(dataList)
            home_fragment_rc_new.addOnScrollListener(object :
                PaginationScrollListener(linearLayoutManager!!) {
                override fun loadMoreItems() {
                    isLoadingNew = true
                    currentPageNew += 1
                    loadNextPageNew(dataList)
                }

                override fun isLoading(): Boolean {
                    return isLoadingNew
                }

                override fun isLastPage(): Boolean {
                    return isLastPageNew
                }
            })
        }
    }

    private fun setFistDataNew(dataList: ArrayList<TopMovie>) {
        newList.addAll(getData(dataList, totalPageNew, currentPageNew))
        adapter_newfilm.setData(newList)
        if (currentPageNew < totalPageNew) {
            adapter_newfilm.addFooterLoading()
        } else {
            isLastPageNew = true
        }
    }

    private fun loadNextPageNew(dataList: ArrayList<TopMovie>) {
        CoroutineScope(Job() + Dispatchers.Main).launch {
            delay(1000)
            var arrayList = getData(dataList, totalPageNew, currentPageNew)

            adapter_newfilm.removeFooterLoading()
            newList.addAll(arrayList)
            adapter_newfilm.notifyDataSetChanged()

            isLoadingNew = false
            if (currentPageNew < totalPageNew) {
                adapter_newfilm.addFooterLoading()
            } else {
                isLastPageNew = true
            }
        }

    }

    private fun setSlide(dataList: ArrayList<TopMovie>) {
        val slidefilm  = ArrayList<TopMovie>()
        for (i in 0 .. 5)
        {
            slidefilm.add(dataList.get(i))
        }

        home_fragment_slide.setSliderAdapter(adapter_slide);
        home_fragment_slide.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        home_fragment_slide.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        home_fragment_slide.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        home_fragment_slide.setIndicatorSelectedColor(Color.WHITE);
        home_fragment_slide.setIndicatorUnselectedColor(Color.GRAY);
        home_fragment_slide.setScrollTimeInSec(3);
        home_fragment_slide.setAutoCycle(true);
        home_fragment_slide.startAutoCycle();
        home_fragment_slide.setOnIndicatorClickListener {
            Log.i("GGG", "onIndicatorClicked: " + home_fragment_slide.getCurrentPagePosition())
        }
        adapter_slide.setData(slidefilm)
    }

    override fun updateViewDataRecommend(dataList: ArrayList<DetailMovie>?) {
        Log.d("KO", "updateViewDataRecommend() called with: dataList = ${dataList.toString()}")
        if (dataList == null) {
            home_fragment_layout_recommend.visibility = View.GONE
        } else {
            adapter_recommend.setData(dataList!!)
        }
    }

    override fun clickOnItem(m: TopMovie) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("movieid", m.id)
        startActivity(intent)
    }

    override fun clickOnItem(m: DetailMovie) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("movieid", m.id)
        startActivity(intent)
    }

    private fun setFistDataPopular(data: ArrayList<TopMovie>) {
        popularList.addAll(getData(data, totalPagePopular, currentPagePopular))
        adapter_popular.setData(popularList)
        if (currentPagePopular < totalPagePopular) {
            adapter_popular.addFooterLoading()
        } else {
            isLastPagePopular = true
        }
    }

    private fun loadNextPagePopular(data: ArrayList<TopMovie>) {
        CoroutineScope(Job() + Dispatchers.Main).launch {
            delay(1000)
            var arrayList = getData(data, totalPagePopular, currentPagePopular)

            adapter_popular.removeFooterLoading()
            popularList.addAll(arrayList)
            adapter_popular.notifyDataSetChanged()

            isLoadingPopular = false
            if (currentPagePopular < totalPagePopular) {
                adapter_popular.addFooterLoading()
            } else {
                isLastPagePopular = true
            }
        }
    }

    private fun getData(
        data: ArrayList<TopMovie>,
        totalPage: Int,
        currentPage: Int
    ): ArrayList<TopMovie> {
        var result = ArrayList<TopMovie>()
        if (totalPage == 1){
            for(i in 0..(data.size-1))
                result.add(data.get(i))
        } else if (currentPage == 1 && totalPage > 1) {
            for (i in 0..9) {
                result.add(data.get(i))
            }
        } else if (currentPage > 1 && currentPage < totalPage){
            for(i in ((currentPage-1)*10)..(currentPage*10-1)){
                result.add(data.get(i))
            }
        } else if (currentPage == totalPage){
            for(i in ((currentPage-1)*10)..(data.size-1)){
                result.add(data.get(i))
            }
        }
        return result
    }

    override fun onDestroy() {
        Log.d("KO", "Home onDestroy() called")
        super.onDestroy()
        presenter.cleanup()
    }

//    override fun onDestroyView() {
//        Log.d("KO", "Home onDestroyView() called")
//        super.onDestroyView()
//    }
//
//    override fun onPause() {
//        Log.d("KO", "Home onPause() called")
//        super.onPause()
//    }
//
//    override fun onResume() {
//        Log.d("KO", "Home onResume() called")
//        super.onResume()
//    }
//
//    override fun onStart() {
//        Log.d("KO", "Home onStart() called")
//        super.onStart()
//    }
//
//    override fun onStop() {
//        Log.d("KO", "Home onStop() called")
//        super.onStop()
//    }

}